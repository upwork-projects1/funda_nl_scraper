import csv

input_file_buy = 'buy.csv'
input_file_rent = 'rent.csv'
output_file = 'final.csv'

cities = set()
data_buy = list()
data_rent = list()

with open(input_file_buy, encoding='utf-8') as f:
    reader = csv.DictReader(f)
    for row in reader:
        row['living_area'] = float(row['living_area'])
        row['price_per_m2'] = float(row['price_per_m2'])
        data_buy.append(dict(row))
        cities.add(row['city'])

print('loaded buy data')

with open(input_file_rent, encoding='utf-8') as f:
    reader = csv.DictReader(f)
    for row in reader:
        try:
            row['living_area'] = float(row['living_area'])
            row['price_per_m2'] = float(row['price_per_m2'])
        except ValueError:
            continue
        data_rent.append(dict(row))
        cities.add(row['city'])

print('loaded rent data')

print(f'total cities: {len(cities)}')

data_with_avg_prices = list()
total_counts = list()
for city in cities:
    data_per_city_buy = [r for r in data_buy if r['city'] == city]
    data_per_city_rent = [r for r in data_rent if r['city'] == city]
    if data_per_city_buy:
        total_count_buy = len(data_per_city_buy)
        avg_price_buy = int(sum([r['price_per_m2'] for r in data_per_city_buy]) / total_count_buy)
    else:
        avg_price_buy = None
        total_count_buy = None
    if data_per_city_rent:
        total_count_rent = len(data_per_city_rent)
        avg_price_rent = int(sum([r['price_per_m2'] for r in data_per_city_rent]) / total_count_rent)
    else:
        avg_price_rent = None
        total_count_rent = None

    data_with_avg_prices.append({'city': city, 'total_count_buy': total_count_buy,
                                 'total_count_rent': total_count_rent,
                                 'avg_price_per_m2_buy': avg_price_buy,
                                 'avg_price_per_m2_rent': avg_price_rent})

with open(output_file, mode='w') as f:
    writer = csv.DictWriter(f, fieldnames=list(data_with_avg_prices[0].keys()))
    writer.writeheader()
    writer.writerows(data_with_avg_prices)
