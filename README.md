# Funda.nl Scraper

To run a scraper, you need Python 3 and pip.

To install all required pip packages, run following command:

``pip install -r requirements.txt``

### How to run the scraper

1. `python refresh_cookies.py`

This command will open Chrome browser and you'll need to manually 
bypass Google Recaptcha (maybe, even twice). 
After this in same browser you need to go to https://www.funda.nl/en/koop/heel-nederland/, 
if that doesn't happen automatically.

2. `scrapy crawl  funda_nl_buy -o buy.csv -t csv`

wait for the scraper to finish crawling. In case, scraper gets blocked, 
crawling process will break.
Now you need to return to step 1 and bypass Captcha again.
In the output logs you can find last page, being scraped.
Copy that link and insert it to `funda_nl_scraper/spiders/funda_nl_buy.py:19`, instead of 
existing line.

Launch scraping again with the same command.

3. `scrapy crawl  funda_nl_rent -o rent.csv -t csv`

wait for the scraper to finish crawling. In case, scraper gets blocked, 
crawling process will break.
Now you need to return to step 1 and bypass Captcha again.
Skip Step 2.
In the output logs you can find last page, being scraped.
Copy that link and insert it to `funda_nl_scraper/spiders/funda_nl_rent.py:19`, instead of 
existing line.

4. `python group_data.py` - will group data from buy.csv and rent.csv by cities and areas.

5. `python group_data_by_city.py` - will group data from buy.csv and rent.csv by cities only.
