import time
import datetime
import json

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException

driver_path = 'drivers/chromedriver_mac'
chrome_options = Options()
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 ' \
             '(KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
chrome_options.add_argument(f'user-agent={user_agent}')
driver = webdriver.Chrome(executable_path=driver_path)

driver.get('https://www.funda.nl/en/huur/heel-nederland/')

try:
    if driver.find_element_by_css_selector('body > div > div > div.content.col-lg-8.col-sm-7 > h1'):
        raise Exception('Scraper blocked. Restart the script.')
except NoSuchElementException:
    ...

# wait 20 minutes (captcha should be solved during 20 min)
start_time = datetime.datetime.now()
check_time = datetime.datetime.now()
captcha_solved = False
while (check_time - start_time) < datetime.timedelta(minutes=20):
    try:
        driver.find_element_by_css_selector("#filters > div.save-search-button.fd-padding > button")
        captcha_solved = True
        print('captcha solved')
        break
    except NoSuchElementException:
        time.sleep(5)

if not captcha_solved:
    driver.close()
    driver.quit()
    raise Exception('Google Recaptcha was not solved.')

cookies = driver.get_cookies()

with open('cookies.json', 'w') as f:
    json.dump(cookies, f)

driver.close()
driver.quit()
