# -*- coding: utf-8 -*-
import json

import scrapy
from scrapy import Request


class FundaNlRentSpider(scrapy.Spider):
    name = 'funda_nl_rent'
    allowed_domains = ['funda.nl']

    def __init__(self):
        super().__init__()
        with open('cookies.json') as f:
            self.cookies = json.load(f)

    def start_requests(self):
        urls = [
            'https://www.funda.nl/en/huur/heel-nederland/'
        ]

        for url in urls:
            yield scrapy.Request(url=url, cookies=self.cookies, callback=self.parse)

    def parse(self, response):
        urls = response.css('.search-result__header-title-col > a::attr(href)').extract()

        for url in urls:
            yield Request('https://www.funda.nl'+url, cookies=self.cookies, callback=self.parse_single_page)

        next_page = response.xpath('//*[@id="content"]/form/div[2]/nav/a/@href').extract()

        if next_page:
            self.logger.info(f'current: {response.url}, next: {next_page}')
            next_page = 'https://www.funda.nl' + next_page[-1]
            yield Request(next_page, cookies=self.cookies, callback=self.parse)

    def parse_single_page(self, response):
        price_per_month = response.css('#content > div > header > div > div > div.object-header__pricing > strong::text').get()
        if price_per_month:
            try:
                price_per_month = float(''.join([i for i in price_per_month if i.isdigit()]))
            except ValueError:
                ...
        else:
            return
        living_area = response.css('#content > div > div.object-primary > section.kenmerken-highlighted > ul'
                                   ' > li:nth-child(2) > span.kenmerken-highlighted__details::text').get()
        if living_area:
            living_area = float(''.join([i for i in living_area.strip('²') if i.isdigit()]))
        else:
            return

        if isinstance(living_area, float) and isinstance(price_per_month, float):
            price_per_m2 = (price_per_month * 12) / living_area
        else:
            price_per_m2 = None

        area = response.css('body > nav > ol > li:nth-child(3) > a::text').get()
        postal_code = response.css('.object-header__subtitle::text').get().split()[0]
        city = response.css('body > nav > ol > li:nth-child(2) > a::text').get()

        return {
            'link': response.url,
            'living_area': living_area,
            'area': area,
            'postal_code': postal_code,
            'city': city,
            'price_per_month': price_per_month,
            'price_per_m2': price_per_m2
        }
